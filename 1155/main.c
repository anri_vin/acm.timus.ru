#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int A, B, C, D, E, F, G, H;
	int total;
	scanf("%d%d%d%d%d%d%d%d", &A, &B, &C, &D, &E, &F, &G, &H);
	if ((A + C + F + H) - (B + D + E + G) != 0)
	{
		printf("%s", "IMPOSSIBLE");
		return 0;
	}
	total = A + B + C + D + E + F + G + H;
	while(A != 0 && B != 0){A--; B--; total -= 2; puts("AB-");}
	while(B != 0 && C != 0){B--; C--; total -= 2; puts("BC-");}
	while(C != 0 && D != 0){C--; D--; total -= 2; puts("CD-");}
	while(D != 0 && A != 0){D--; A--; total -= 2; puts("DA-");}
	while(E != 0 && F != 0){E--; F--; total -= 2; puts("EF-");}
	while(F != 0 && G != 0){F--; G--; total -= 2; puts("FG-");}
	while(G != 0 && H != 0){G--; H--; total -= 2; puts("GH-");}
	while(H != 0 && E != 0){H--; E--; total -= 2; puts("HE-");}
	while(A != 0 && E != 0){A--; E--; total -= 2; puts("AE-");}
	while(B != 0 && F != 0){B--; F--; total -= 2; puts("BF-");}
	while(C != 0 && G != 0){C--; G--; total -= 2; puts("CG-");}
	while(D != 0 && H != 0){D--; H--; total -= 2; puts("DH-");}
	
	while(A != 0 && G != 0){A--; G--; total -= 2; puts("BF+\nAB-\nGF-");}
	while(B != 0 && H != 0){B--; H--; total -= 2; puts("CG+\nBC-\nHG-");}
	while(C != 0 && E != 0){C--; E--; total -= 2; puts("DH+\nCD-\nEH-");}
	while(D != 0 && F != 0){D--; F--; total -= 2; puts("AE+\nDA-\nFE-");}
	return 0;
}
