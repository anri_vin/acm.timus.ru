#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct symbol
{
	struct symbol *previous;
	struct symbol *next;
	char value;
};

struct symbol *rm(struct symbol *message)
{
	struct symbol *ptr, *first;
	ptr = message;
	first = ptr;
	while(1)
	{
		if(ptr->next == NULL)
			break;
		if (ptr->value == (ptr->next)->value)
		{
			if (ptr->previous == NULL)
			{
				ptr = (ptr->next)->next;
				ptr->previous = NULL;
				first = ptr;
			}
			else
			{
				((ptr->next)->next)->previous = ptr->previous;
				(ptr->previous)->next = (ptr->next)->next;
				ptr = ptr->previous;
			}
		}
		else
			ptr = ptr->next;
	}
	return first;
}

int main(int argc, char **argv)
{
	int i = 0;
	struct symbol symbols[200001], *tmp_ptr;
	memset(symbols, 0, sizeof(struct symbol) * 200001);
	while(1)
	{
		scanf("%c", &symbols[i].value);
		if (i != 0)
			symbols[i].previous = &symbols[i-1];
		if (symbols[i].value == '\n')
		{
			symbols[i].value = 0;
			break;
		}
		else
			symbols[i].next = &symbols[i+1];
		i++;
	}
	tmp_ptr = rm(&symbols[0]);
	while(1)
	{
		if (tmp_ptr->value != 0)
			printf("%c", tmp_ptr->value);
		if (tmp_ptr->next != NULL)
			tmp_ptr = tmp_ptr->next;
		else
			break;
	}
	return 0;
}