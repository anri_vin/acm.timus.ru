#include <stdio.h>
#include <stdlib.h>

static int cmp(const void *a, const void *b) {
	int x = *(const int*)a;
	int y = *(const int*)b;
	return x > y ? -1 : x < y ? 1 : 0;
}


int main(int argc, char **argv)
{
	int i, count, array[25000] = {0};
	scanf("%d", &count);
	for (i = 0; i < count; i++)
		scanf("%d", &array[i]);
	qsort(array, count, sizeof(int), cmp);
	for (i = 0; i < count; i++)
		printf("%d\n", array[i]);
	return 0;
}
