#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
	char name[21];
	int played_with[300];
	int counter;
	int isenbaev;
	int length;
};

int get_index(struct person *persons, int Isenbaev, int N, int length);
void sort(struct person *persons, int N);
int search_or_insert(struct person *persons, int *N, char *name, int insert);
void link(struct person *persons, int num1, int num2);

int main(int argc, char **argv)
{
	int N, i, persons_counter = 0, Isenbaev;
	struct person persons[300];
	char temp1[65], *name1, *name2, *name3;
	
	memset(persons, 0, sizeof(struct person) * 300);
	scanf("%d\n", &N);
	for (i = 0; i < N; i++)
	{
		int num1, num2, num3;
		fgets(temp1, 65, stdin);
		name1 = strtok(temp1, " \n");
		name2 = strtok(NULL, " \n");
		name3 = strtok(NULL, " \n");
		num1 = search_or_insert(persons, &persons_counter, name1, 1);
		num2 = search_or_insert(persons, &persons_counter, name2, 1);
		num3 = search_or_insert(persons, &persons_counter, name3, 1);
		link(persons, num1, num2);
		link(persons, num1, num3);
		link(persons, num2, num3);
	}
	Isenbaev = search_or_insert(persons, &persons_counter, "Isenbaev", 0);
	if (Isenbaev == -1)
	{
		sort(persons, persons_counter);
		for (i = 0; i < persons_counter; i++)
			printf("%s undefined\n", persons[i].name);
		return 0;
	}
	for (i = 0; i < persons_counter; i++)
	{
		if (i == Isenbaev)
			continue;
		persons[i].isenbaev = get_index(persons, Isenbaev, i, 0);
	}
	persons[Isenbaev].isenbaev = 0;
	sort(persons, persons_counter);
	for (i = 0; i < persons_counter; i++)
		if (persons[i].isenbaev == 9999)
			printf("%s undefined\n", persons[i].name);
		else	
			printf("%s %d\n", persons[i].name, persons[i].isenbaev);
	return 0;
}

int search_or_insert(struct person *persons, int *N, char *name, int insert)
{
	int i;
	for (i = 0; i < *N; i++)
		if (strcmp(persons[i].name, name) == 0)
			return i;
	if (insert == 0)
		return -1;
	strcpy(persons[*N].name, name);
	persons[*N].length = 9999;
	*N = *N + 1;
	return *N - 1;
}

void link(struct person *persons, int num1, int num2)
{
	int i, flag = 0;
	for (i = 0; i < persons[num1].counter; i++)
		if (persons[num1].played_with[i] == num2)
			flag = 1;
	if (flag == 0)
	{
		persons[num1].played_with[persons[num1].counter] = num2;
		persons[num1].counter++;
		persons[num2].played_with[persons[num2].counter] = num1;
		persons[num2].counter++;
	}
}

int get_index(struct person *persons, int start, int target, int length)
{
	int i, way = 9999, temp;
	if (persons[start].length < length)
		return 0;
	persons[start].length = length;
	length++;
	for (i = 0; i < persons[start].counter; i++)
	{
		if (persons[start].played_with[i] == target)
			return length;
		temp = get_index(persons, persons[start].played_with[i], target, length);
		if (temp != 0 && temp < way)
			way = temp;
	}
	return way;
}

void sort(struct person *persons, int N)
{
	struct person temp;
	int i, j, k;
	for (i = 0; i < N - 1; i++)
		for (j = i + 1; j < N; j++)
			for (k = 0; k < 21; k++)
			{
				if (persons[i].name[k] < persons[j].name[k])
					break;
				if (persons[i].name[k] > persons[j].name[k])
				{
					temp = persons[i];
					persons[i] = persons[j];
					persons[j] = temp;
					break;
				}
			}
}