#include <stdio.h>
#include <stdlib.h>

static int cmp(const void *a, const void *b) {
	int x = *(const int*)a;
	int y = *(const int*)b;
	return x < y ? -1 : x > y ? 1 : 0;
}


int main(int argc, char **argv)
{
	char temp;
	int request, data[100000] = {0}, elem_count, req_count, i = 0;
	scanf("%d", &elem_count);
	for (i = 0; i < elem_count; i++)
		scanf("%d", &data[i]);
	scanf("%s", &temp);
	scanf("%d", &req_count);
	qsort(data, elem_count, sizeof(int), cmp);
	for (i = 0; i < req_count; i++)
	{
		scanf("%d", &request);
		printf("%d\n", data[request - 1]);
	}
	return 0;
}
