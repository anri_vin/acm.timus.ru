#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	int i, j, n, min, first, second;
	int mask_bits[21] = {0};
	int stones[20] = {0};
	scanf("%d", &n);
	min = 0;
	for (i = 0; i < n; i++)
	{
       scanf("%d", &stones[i]);
		min += stones[i];
	}
	while (mask_bits[0] != 1)
	{
		j = n;
		while(mask_bits[j] == 1)
		{
			mask_bits[j] = 0;
			j--;
		}
		mask_bits[j] = mask_bits[j] + 1;
		first= 0;
		second = 0;
		for (i = 1; i <= n; i++)
		{
       	if (mask_bits[i] == 0)
				second += stones[i - 1];
			else
				first += stones[i - 1];
		}
		if (min > (second > first ? second - first : first - second))
          min = (second > first ? second - first : first - second);
	}
	printf("%d", min);
	return 0;
}