#include <stdio.h>

int main()
{
	int a;
	int n;
	scanf("%d", &n);
	int sum = 0;
	int max = 0;
	while (n--)
	{
		scanf("%d", &a);
		sum += a;
		if (sum < 0)
			sum = 0;
		max = (sum > max ? sum : max);
	}
	printf("%d", max);
	return 0;
}