#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

struct point
{
	int number;
	int x;
	int y;
	double angle;
	double length;
};

void remove_element(struct point *array, int index, int array_length)
{
   int i;
   for(i = index; i < array_length - 1; i++) array[i] = array[i + 1];
}

int main(int argc, char **argv)
{
	int N, i, j;
	double eps = 1e-8;
	struct point first_point, temp;
	struct point *points;
	
	scanf("%d", &N);
	points = malloc(sizeof(struct point) * N);
	for (i = 0; i < N; i++)
	{
		scanf("%d%d", &points[i].x, &points[i].y);
		points[i].number = i + 1;
	}
	first_point = points[0];
	remove_element(points, first_point.number - 1, N);
	N--;
	for (i = 0; i < N; i++)
	{
		points[i].angle = atan2((double)(points[i].y - first_point.y), (double)(points[i].x - first_point.x)) * 180 / 3.141593;
		if (points[i].x - first_point.x < 0)
			points[i].angle = points[i].angle < 0 ? -180 - points[i].angle : 180 - points[i].angle;
		points[i].length = sqrt((double)pow(points[i].x - first_point.x, 2) + pow(points[i].y - first_point.y, 2));
	}
	for (i = 0; i < N - 1; i++)
		for (j = i; j < N; j++)
			if (points[i].angle - points[j].angle > eps)
			{
				temp = points[i];
				points[i] = points[j];
				points[j] = temp;
			}
			else if (points[i].angle - points[j].angle < eps && points[i].angle - points[j].angle > 0) 
			{
				if (points[i].length - points[j].length > eps)
				{
					temp = points[i];
					points[i] = points[j];
					points[j] = temp;
				}
			}
	printf("%d\n1\n", N + 1);
	
	for (i = 0; i < N; i++)
		printf("%d\n", points[i].number);
	return 0;
}
