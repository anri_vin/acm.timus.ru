#include <stdio.h>
#include <stdlib.h>

int members[101][101] = {{0}};
int answer[100] = {0};

void clear(int num)
{
	int i;
	for (i = 0; i < 101; i++)
		members[i][num] = 0;
}

int is_checked(int num)
{
	int i;
	for (i = 0; i < 100; i++)
		if (answer[i] == num)
			return 1;
	return 0;
}

void topological_sort(int members[101][101], int N)
{
	int i, j, k = 0, sum, flag;
	while(1)
	{
		flag = 0;
		for (i = 1; i <= N; i++)
		{
			sum = 0;
			for (j = 1; j <= N; j++)
				sum += members[i][j];
			if (sum == 0 && is_checked(i) == 0)
			{
				clear(i);
				answer[k] = i;
				k++;
			}
			else if (sum != 0)
				flag = 1;
		}
		if (flag == 0)
			break;
	}
}

int main(int argc, char **argv)
{
	int N, i, child;
	
	scanf("%d", &N);
	for (i = 1; i <= N; i++)
	{
		while(1)
		{
			scanf("%d", &child);
			if (child == 0)
				break;
			members[i][child] = 1;
		}
	}
	topological_sort(members, N);
	for (i = 99; i >= 0; i--)
		if (answer[i] != 0)
			printf("%d ", answer[i]);
	return 0;
}
