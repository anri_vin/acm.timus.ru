#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

struct point
{
	int number;
	long int x;
	long int y;
	double angle;
};

void remove_element(struct point *array, int index, int array_length)
{
   int i;
   for(i = index; i < array_length - 1; i++) array[i] = array[i + 1];
}

int main(int argc, char **argv)
{
	int N, i, j;
	struct point left_point, temp;
	struct point *points;
	
	left_point.x = LONG_MAX;
	left_point.y = LONG_MAX;
	scanf("%d", &N);
	points = malloc(sizeof(struct point) * N);
	for (i = 0; i < N; i++)
	{
		scanf("%ld%ld", &points[i].x, &points[i].y);
		points[i].number = i + 1;
		if (points[i].x < left_point.x)
			left_point = points[i];
		else if (points[i].x == left_point.x && points[i].y < left_point.y)
			left_point = points[i];
	}
	remove_element(points, left_point.number - 1, N);
	N--;
	for (i = 0; i < N; i++)
		points[i].angle = atan2((double)(points[i].y - left_point.y), (double)(points[i].x - left_point.x));
	for (i = 0; i < N - 1; i++)
		for (j = i; j < N; j++)
			if (points[i].angle < points[j].angle)
			{
				temp = points[i];
				points[i] = points[j];
				points[j] = temp;
			}
	printf("%d %d", points[N / 2].number, left_point.number);
	return 0;
}