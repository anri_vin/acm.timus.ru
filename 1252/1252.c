#include <stdio.h>
#include <stdlib.h>

struct pair
{
    int value, number;
};

static int cmp_up(const void *a, const void *b)
{
    struct pair x = *(const struct pair*)a;
    struct pair y = *(const struct pair*)b;
    return x.value < y.value ? -1 : x.value > y.value ? 1 : 0;
}

static int cmp_down(const void *a, const void *b)
{
    struct pair x = *(const struct pair*)a;
    struct pair y = *(const struct pair*)b;
    return x.value < y.value ? 1 : x.value > y.value ? -1 : 0;
}

int gcd(int a, int b)
{
    int temp;
    while (b != 0)
    {
        temp = a % b;
        a = b;
        b = temp;
    }
    return a;
}

int main(int argc, char **argv)
{
    int N, i, g, max_up = 0, max_down = 0;
    struct pair arr[130000];

    scanf("%d", &N);
    for (i = 0; i < N; i++)
    {
        scanf("%d", &arr[i].value);
        arr[i].number = i;
    }
    
    g = 0;
    qsort(arr, N, sizeof(struct pair), cmp_up);
    for (i = 0; i < N; i++)
        g = gcd(g, abs(i - arr[i].number));
    max_up = N - 1;
    if (g != 0)
        max_up = g - 1;

    g = 0;
    qsort(arr, N, sizeof(struct pair), cmp_down);
    for (i = 0; i < N; i++)
        g = gcd(g, abs(i - arr[i].number));
    max_down = N - 1;
    if (g != 0)
        max_down = g - 1;

    printf("%d", max_up > max_down ? max_up : max_down);
    return 0;
}
