#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
	int friends[100];
	int friends_count;
	int color;
};

void colorize(struct person *persons, int i, int color)
{
	int j, set_color;
	if (persons[i].color != 0)
		return;
	persons[i].color = color;
	set_color = (color == 1 ? 2 : 1);
	for (j = 0; j < persons[i].friends_count; j++)
	{
		colorize(persons, persons[i].friends[j] - 1, set_color);
	}
}

int main(int argc, char **argv)
{
	int N, i, j, count = 0;
	struct person persons[100];
	
	memset(&persons, 0, sizeof(struct person) * 100);
	scanf("%d", &N);
	for (i = 0; i < N; i++)
	{
		j = 0;
		while(1)
		{
			scanf("%d", &persons[i].friends[j]);
			if (persons[i].friends[j] == 0)
				break;
			persons[i].friends_count++;
			j++;
		}
	}
	for (i = 0; i < N; i++)
		colorize(persons, i, 1);
	for (i = 0; i < N; i++)
		if (persons[i].color == 1)
			count++;
	printf("%d\n", count);
	for (i = 0; i < N; i++)
		if (persons[i].color == 1)
			printf("%d ", i+1);
	return 0;
}
