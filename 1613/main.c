#include <stdio.h>
#include <stdlib.h>

struct pair
{
	int num;
	int value;
};

static int cmp(const void *a, const void *b) {
	struct pair x = *(const struct pair *)a;
	struct pair y = *(const struct pair *)b;
	return x.value < y.value ? -1 : x.value > y.value ? 1 : x.num > y.num ? 1 : -1;
}

int search(struct pair *pairs, int min, int max, int left, int right, int value)
{
	int mid;
	if (max < min) 
		return -1;
    mid = min + (max - min) / 2;
	if (pairs[mid].value > value)
		return search(pairs, min, mid - 1, left, right, value);
	else if (pairs[mid].value < value)
		return search(pairs, mid + 1, max, left, right, value);
	else if (pairs[mid].num >= left)
		if (pairs[mid].num <= right) 
			return mid;
		else 
			return search(pairs, min, mid - 1, left, right, value);
	else
		return search(pairs, mid + 1, max, left, right, value);
}

int main(int argc, char **argv)
{
	int N, N_q, i, left, right, value, result;
	char answer[70000] = {0};
	struct pair pairs[70000];
	scanf("%d", &N);
	for (i = 0; i < N; i++)
	{
		scanf("%d", &pairs[i].value);
		pairs[i].num = i + 1;
	}
	qsort(pairs, N, sizeof(struct pair), cmp);
	scanf("%d", &N_q);
	for (i = 0; i < N_q; i++)
	{
		scanf("%d%d%d", &left, &right, &value);
		result = search(pairs, 0, N-1, left, right, value);
		*(answer + i) = '0';
		if (result != -1)
			*(answer + i) = '1';
	}
	printf("%s", answer);
	return 0;
}
