#include <stdio.h>
#include <stdlib.h>

typedef struct{
	int num;
	int value;
}pair;

int cmp(const void *a, const void *b)
{
	const pair x = *(const pair*)a;
	const pair y = *(const pair*)b;
	return x.value > y.value ? 1 : x.value < y.value ? -1 : x.num > y.num ? 1 : -1;
}

int main(int argc, char **argv)
{
	int N, i, j, k, flag;
	pair pairs[10002], temp;
	
	scanf("%d", &N);
	for(i = 0; i < N; i++)
	{
		scanf("%d", &pairs[i].value);
		pairs[i].num = i + 1;
	}
	qsort(pairs, N, sizeof(pair), cmp);
	i = 0; j = N - 1;
	if (N == 1)
	{ 
		for (i = 0; i < pairs[0].value; i++) 
			printf("%d ", 1);
		return 0;
	}
	while (1)
	{
		printf("%d %d ", pairs[j].num, pairs[i].num);
		pairs[i].value--;
		pairs[j].value--;
		if (pairs[i].value == 0) 
			i++;
		if (pairs[j].value == 0) 
			j--;
		if (i >= j) 
		{
			while (pairs[i].value > 0)
			{
				printf("%d ", pairs[i].num); 
				pairs[i].value--;
			}
			while (pairs[j].value > 0)
			{
				printf("%d ", pairs[j].num);
				pairs[j].value--;
			}
			break;
		}		
		for(k = i; k < j; k++)
		{
			flag = 0;
			if (cmp((pairs + k), (pairs + k + 1)) > 0) 
			{
				temp = pairs[k];
				pairs[k] = pairs[k + 1];
				pairs[k + 1] = temp;
				flag = 1;
			}
			if (flag == 0) 
				break;
		}
		for(k = j; k > i; k--)
		{
			flag = 0;
			if (cmp((pairs + k), (pairs + k - 1)) < 0) 
			{
				temp = pairs[k];
				pairs[k] = pairs[k - 1];
				pairs[k - 1] = temp;
				flag = 1;
			}
			if (flag == 0) 
				break;
		}
	}
	return 0;
}
