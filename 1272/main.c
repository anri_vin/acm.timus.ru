#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct isle
{
	int color;
	int *tunnels;
	int tunnels_count;
};

void colorize(struct isle *isles, int i)
{
	int j;
	
	if (isles[i].color == 1)
		return;
	isles[i].color = 1;
	for (j = 0; j < isles[i].tunnels_count; j++)
		colorize(isles, isles[i].tunnels[j]);
}

int main(int argc, char **argv)
{
	int isles_count, tunnels_count, bridges_count;
	int i;
	int temp1, temp2;
	int counter = 0;
	struct isle *isles;
	
	scanf("%d%d%d", &isles_count, &tunnels_count, &bridges_count);
	isles = malloc(sizeof(struct isle) * isles_count);
	memset(isles, 0, sizeof(struct isle) * isles_count);
	
	for (i = 0; i < tunnels_count; i++)
	{
		scanf("%d%d", &temp1, &temp2);
		if (isles[temp1 - 1].tunnels_count == 0)
			isles[temp1 - 1].tunnels = malloc(sizeof(int));
		else
			isles[temp1 - 1].tunnels = realloc(isles[temp1 - 1].tunnels, sizeof(int) * (isles[temp1 - 1].tunnels_count + 1));
		
		isles[temp1 - 1].tunnels[isles[temp1 - 1].tunnels_count] = temp2 - 1;
		isles[temp1 - 1].tunnels_count++;
		
		if (isles[temp2 - 1].tunnels_count == 0)
			isles[temp2 - 1].tunnels = malloc(sizeof(int));
		else
			isles[temp2 - 1].tunnels = realloc(isles[temp2 - 1].tunnels, sizeof(int) * (isles[temp2 - 1].tunnels_count + 1));
		
		isles[temp2 - 1].tunnels[isles[temp2 - 1].tunnels_count] = temp1 - 1;
		isles[temp2 - 1].tunnels_count++;
	}
	for (i = 0; i < bridges_count; i++)
	{
		scanf("%d%d", &temp1, &temp2);
	}
	for (i = 0; i < isles_count; i++)
	{
		if (isles[i].color != 1)
		{
			colorize(isles, i);
			counter++;
		}
	}
	printf("%d", (counter - 1));
	return 0;
}
