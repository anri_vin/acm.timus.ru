#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLOCK_SIZE 100

#pragma pack(push, 1)
struct block
{
	int *values;
	int count;
	struct block *next;
};
#pragma pack(pop)

int main(int argc, char **argv)
{
	int A, B, N, i;
	char str[22];
	struct block stacks[1000], *temp, *temp1;
	
	memset(stacks, 0, sizeof(struct block) * 1000);
	scanf("%d\n", &N);
	for (i = 0; i < N; i++)
	{
		fgets(str, 22, stdin);
		if (strcmp(strtok(str, " "), "PUSH") == 0)
		{
			A = atoi(strtok(NULL, " \n"));
			B = atoi(strtok(NULL, " \n"));
			temp = &stacks[A - 1];
			while(temp->next != NULL)	
			{	
				temp = temp->next;
			}
			if (temp->count == BLOCK_SIZE && temp->next == NULL)
			{
				temp->next = malloc(sizeof(struct block));
				temp = temp->next;
				memset(temp, 0, sizeof(struct block));
				stacks[A - 1].blocks_count++;
			}
			if (temp->values == NULL)
				temp->values = malloc(sizeof(int));
			else
				temp->values = realloc(temp->values, sizeof(int) * (temp->count + 1));
			temp->values[temp->count] = B;
			temp->count++;
		}
		else
		{
			A = atoi(strtok(NULL, " \n"));
			temp = &stacks[A - 1];
			temp1 = temp;
			while(temp->next != NULL)	
			{	
				temp1 = temp;
				temp = temp->next;
			}
			printf("%d\n", temp->values[temp->count - 1]);
			temp->values = realloc(temp->values, sizeof(int) * temp->count);
			temp->count--;
			if (temp1 != temp && temp->count == 0)
			{
				free(temp->values);
				free(temp);
				temp1->next = NULL;
				stacks[A - 1].blocks_count--;
			}
			
		}
	}
	return 0;
}
