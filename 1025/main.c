#include <stdio.h>
#include <stdlib.h>

static int cmp(const void *a, const void *b) {
	int x = *(const int*)a;
	int y = *(const int*)b;
	return x < y ? -1 : x > y ? 1 : 0;
}

int main(int argc, char **argv)
{
	int i, count, groups[101] = {0}, result = 0;
	scanf("%d", &count);
	for (i = 0; i < count; i++)
		scanf(" %d", &groups[i]);
	qsort(groups, count, sizeof(int), cmp);
	for (i = 0; i < count / 2 + 1; i++)
	{
		result += groups[i] / 2 + 1;
	}
	printf("%d\n", result);
	return 0;
}
