#include <stdio.h>
#include <stdlib.h>

struct leaf
{
	struct leaf *left;
	struct leaf *right;
	int value;
};

void insert_value(struct leaf *ptr, int value)
{
	if (value < ptr->value)
	{
		if (ptr->left == NULL)
		{
			ptr->left = malloc(sizeof(struct leaf));
			(ptr->left)->value = value;
			(ptr->left)->left = NULL;
			(ptr->left)->right = NULL;
		}
		else
			insert_value(ptr->left, value);
	}
	else
	{
		if (ptr->right == NULL)
		{
			ptr->right = malloc(sizeof(struct leaf));
			(ptr->right)->value = value;
			(ptr->right)->left = NULL;
			(ptr->right)->right = NULL;
		}
		else
			insert_value(ptr->right, value);
	}
}

void print_req(struct leaf *ptr)
{
	if (ptr->right != NULL)
		print_req(ptr->right);
	if (ptr->left != NULL)
		print_req(ptr->left);
	printf("%d ", ptr->value);
}

int main(int argc, char **argv)
{
	int N, center = 0, i = 0, members[3000] = {0};
	struct leaf tree;
	
	scanf("%d", &N);
	for (i = 0; i < N; i++)
		scanf("%d", &members[i]);
	tree.value = members[N - 1];
	tree.left = NULL;
	tree.right = NULL;
	while(members[center] < tree.value)
		center++;
	for (i = center - 1; i >= 0; i--)
		insert_value(&tree, members[i]);
	for (i = N - 2; i >= center; i--)
		insert_value(&tree, members[i]);
	print_req(&tree);
	return 0;
}
