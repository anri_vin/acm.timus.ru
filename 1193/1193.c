#include <stdio.h>
#include <stdlib.h>


struct stud
{
    int T1, T2, T3;
};

int main(int argc, char **argv)
{
    int N, max = 0, start = 0, i, j;
    struct stud studs[40], temp;
    scanf("%d", &N);
    for (i = 0; i < N; i++)
        scanf("%d %d %d", &studs[i].T1, &studs[i].T2, &studs[i].T3); 
    for (i = 0; i < N; i++)
        for (j = i + 1; j < N; j++)
            if (studs[i].T1 > studs[j].T1)
            {   
                temp = studs[i];
                studs[i] = studs[j];
                studs[j] = temp;
            }
    start = studs[0].T1 + studs[0].T2;
    if (start > studs[0].T3)
    {
        max = start - studs[0].T3;
        for (j = 1; j < N; j++)
            studs[j].T3 += max;
    }
    for (i = 1; i < N; i++)
    {
        if (start < studs[i].T1) 
            start = studs[i].T1;
        start += studs[i].T2;
        if (start > studs[i].T3)
        {
            max += start - studs[i].T3;
            for (j = i + 1; j < N; j++)
                studs[j].T3 += start - studs[i].T3;
        }
    }
    printf("%d\n", max);   
    return 0;
}

